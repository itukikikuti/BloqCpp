#pragma once

namespace bloq
{
	enum class Priority
	{
		Prefix = 4,
		MulDivMod = 3,
		LessGreater = 2,
		Equals = 1,
		Lowest = 0,
	};

	class Expression
	{
	public:
		static std::unique_ptr<Expression> Parse(const Priority& priority, const Interpreter& interpreter, int& i);
        virtual ~Expression() = default;
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const = 0;
		virtual void ToString(std::ostream& out) const = 0;
        
    protected:
        static Priority GetInfixPriority(const Token& token);
	};

	std::ostream& operator<<(std::ostream& out, const Expression& expression);

	class PrefixExpression : public Expression
	{
	public:
		std::unique_ptr<Expression> right = nullptr;
	};

	class InfixExpression : public Expression
	{
	public:
        static std::unique_ptr<InfixExpression> Parse(std::unique_ptr<Expression>& left, const Interpreter& interpreter, int& i);
		std::unique_ptr<Expression> left = nullptr;
		std::unique_ptr<Expression> right = nullptr;
	};

	class IncrementExpression : public PrefixExpression
	{
	public:
		IncrementExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
	};

	class IntLiteralExpression : public Expression
	{
	public:
		IntLiteralExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        int value;
	};

	class StringLiteralExpression : public Expression
	{
	public:
		StringLiteralExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::string value;
	};

	class IdentifierExpression : public Expression
	{
	public:
		IdentifierExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::string identifier;
	};

	class ModExpression : public InfixExpression
	{
	public:
		ModExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
	};

	class EqualExpression : public InfixExpression
	{
	public:
		EqualExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
	};

	class LessExpression : public InfixExpression
	{
	public:
		LessExpression(const Interpreter& interpreter, int& i);
		virtual std::shared_ptr<Value> Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
	};
}
