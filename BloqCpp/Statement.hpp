#pragma once

namespace bloq
{
	class Statement
	{
	public:
		static std::unique_ptr<Statement> Parse(const Interpreter& interpreter, int& i);
        virtual ~Statement() = default;
		virtual void Evaluate(Interpreter& interpreter) const = 0;
		virtual void ToString(std::ostream& out) const = 0;
	};

	std::ostream& operator<<(std::ostream& out, const Statement& statement);

	class RootStatement : public Statement
	{
	public:
		RootStatement(const Interpreter& interpreter, int& i);
		virtual void Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::vector<std::unique_ptr<Statement>> statements;
	};

	class BlockStatement : public Statement
	{
	public:
		BlockStatement(const Interpreter& interpreter, int& i, const std::initializer_list<TokenType>& endTokenTypes);
		virtual void Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::vector<std::unique_ptr<Statement>> statements;
	};

	class IfStatement : public Statement
	{
	public:
		struct Pair
		{
			Pair(std::unique_ptr<Expression>& condition, std::unique_ptr<BlockStatement>& then)
			{
				this->condition = std::move(condition);
				this->then = std::move(then);
			}
            std::unique_ptr<Expression> condition = nullptr;
            std::unique_ptr<BlockStatement> then = nullptr;
		};
		IfStatement(const Interpreter& interpreter, int& i);
		virtual void Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::vector<Pair> pairs;
        std::unique_ptr<BlockStatement> elseStatements = nullptr;
	};

    class WhileStatement : public Statement
    {
    public:
        WhileStatement(const Interpreter& interpreter, int& i);
        virtual void Evaluate(Interpreter& interpreter) const override;
        virtual void ToString(std::ostream& out) const override;
        std::unique_ptr<Expression> condition = nullptr;
        std::unique_ptr<BlockStatement> then = nullptr;
    };

	class PrintStatement : public Statement
	{
	public:
		PrintStatement(const Interpreter& interpreter, int& i);
		virtual void Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::unique_ptr<Expression> expression = nullptr;
	};

	class ExpressionStatement : public Statement
	{
	public:
		ExpressionStatement(const Interpreter& interpreter, int& i);
		virtual void Evaluate(Interpreter& interpreter) const override;
		virtual void ToString(std::ostream& out) const override;
        std::unique_ptr<Expression> expression = nullptr;
	};

    class IntStatement : public Statement
    {
    public:
        IntStatement(const Interpreter& interpreter, int& i);
        virtual void Evaluate(Interpreter& interpreter) const override;
        virtual void ToString(std::ostream& out) const override;
        std::string identifier;
    };
}
