#pragma once

namespace bloq
{
	struct Value
	{
        virtual ~Value() = default;
		virtual void ToString(std::ostream& out) const = 0;
	};

	std::ostream& operator<<(std::ostream& out, const Value& value);

	struct IntValue : public Value
	{
		IntValue(const int& value = 0);
		virtual void ToString(std::ostream& out) const override;
        int value;
	};

	struct BoolValue : public Value
	{
		BoolValue(const bool& value = false);
		virtual void ToString(std::ostream& out) const override;
        bool value;
	};

	struct StringValue : public Value
	{
		StringValue(const std::string& value = "");
		virtual void ToString(std::ostream& out) const override;
        std::string value;
	};
}
