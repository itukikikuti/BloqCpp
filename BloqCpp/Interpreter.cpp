#include "Header.hpp"

namespace bloq
{
	Interpreter::Interpreter()
	{
		tokenPatterns = {
			TokenPattern(TokenType::Eof,			R"()"),
			TokenPattern(TokenType::NewLine,		R"(\\n)"),
			TokenPattern(TokenType::Assign,			R"(=)"),
			TokenPattern(TokenType::Percent,		R"(%)"),
			TokenPattern(TokenType::Equal,			R"(==)"),
			TokenPattern(TokenType::Less,			R"(<)"),
			TokenPattern(TokenType::Increment,		R"(\+\+)"),
			TokenPattern(TokenType::If,				R"(if)"),
			TokenPattern(TokenType::Else,			R"(else)"),
			TokenPattern(TokenType::Elseif,			R"(elseif)"),
			TokenPattern(TokenType::While,			R"(while)"),
			TokenPattern(TokenType::End,			R"(end)"),
			TokenPattern(TokenType::Int,			R"(int)"),
			TokenPattern(TokenType::IntLiteral,		R"([0-9]+)"),
			TokenPattern(TokenType::StringLiteral,	R"(".+")"),
			TokenPattern(TokenType::Print,			R"(print)"),
			TokenPattern(TokenType::Identifier,		R"(([a-z]|_)+)"),
		};
	}

	void Interpreter::Tokenize(std::string code)
	{
		code += "\n";

		// 改行を可視化する(下でホワイトスペースで分割するので前後にスペースを挿入)
		code = std::regex_replace(code, std::regex(R"(\n+)"), " \\n ");

		// ホワイトスペースで分割する
		std::regex pattern(R"(\s+)");
		std::sregex_token_iterator itr(code.begin(), code.end(), pattern, -1), end;

		while (itr != end)
		{
			std::string literal = *itr;
			tokens.push_back(ToToken(literal));
			itr++;
		}
	}

	void Interpreter::Parse()
	{
		int i = 0;
		root = std::make_unique<RootStatement>(*this, i);
#if defined(_DEBUG) or defined(DEBUG)
		std::cout << "Parse" << std::endl << *root << std::endl;
#endif
	}

	void Interpreter::Evaluate()
	{
		root->Evaluate(*this);
	}

    Token Interpreter::GetToken(const int& i) const
    {
        if (tokens.size() > i)
            return tokens[i];

        return Token(TokenType::Eof, "");
    }

	Token Interpreter::ToToken(const std::string& literal)
	{
		for (auto tokenPattern : tokenPatterns)
		{
			if (std::regex_match(literal, std::regex(tokenPattern.pattern)))
			{
				return Token(tokenPattern.tokenType, literal);
			}
		}
        
        throw std::runtime_error("");
	}
}
