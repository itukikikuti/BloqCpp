#include "Header.hpp"

namespace bloq
{
	std::unique_ptr<Statement> Statement::Parse(const Interpreter& interpreter, int& i)
	{
		switch (interpreter.GetToken(i).type)
		{
		case TokenType::If:
			return std::make_unique<IfStatement>(interpreter, i);
        case TokenType::While:
            return std::make_unique<WhileStatement>(interpreter, i);
		case TokenType::Print:
			return std::make_unique<PrintStatement>(interpreter, i);
        case TokenType::Int:
            return std::make_unique<IntStatement>(interpreter, i);
        default:
            return std::make_unique<ExpressionStatement>(interpreter, i);
		}
	}

	std::ostream& operator<<(std::ostream& out, const Statement& statement)
	{
		statement.ToString(out);
		return out;
	}

	RootStatement::RootStatement(const Interpreter& interpreter, int& i)
	{
		while (interpreter.GetToken(i).type != TokenType::Eof)
		{
			auto statement = Statement::Parse(interpreter, i);

			if (statement)
				statements.push_back(std::move(statement));

			++i;
		}
	}

	void RootStatement::Evaluate(Interpreter& interpreter) const
	{
		for (auto& statement : statements)
		{
			statement->Evaluate(interpreter);
		}
	}

	void RootStatement::ToString(std::ostream& out) const
	{
		for (auto& statement : statements)
		{
			out << *statement << std::endl;
		}
	}

	BlockStatement::BlockStatement(const Interpreter& interpreter, int& i, const std::initializer_list<TokenType>& endTokenTypes)
	{
		++i;
		auto& t = endTokenTypes;

		// find==endでendTokenTypesの中にcurrentTokenがあるか調べてる
		while (std::find(t.begin(), t.end(), interpreter.GetToken(i).type) == t.end() and interpreter.GetToken(i).type != TokenType::Eof)
		{
			auto statement = Statement::Parse(interpreter, i);

			if (statement)
				statements.push_back(std::move(statement));

			++i;
		}
	}

	void BlockStatement::Evaluate(Interpreter& interpreter) const
	{
		for (auto& statement : statements)
		{
			statement->Evaluate(interpreter);
		}
	}

	void BlockStatement::ToString(std::ostream& out) const
	{
		for (auto& statement : statements)
		{
			out << *statement << std::endl;
		}
	}

	IfStatement::IfStatement(const Interpreter& interpreter, int& i)
	{
        do
        {
            ++i;
            auto condition = Expression::Parse(Priority::Lowest, interpreter, i);
            ++i;
            // make_uniqueでinitializer_listが使えねえからこうなった(きったねえ)
            auto then = std::unique_ptr<BlockStatement>(new BlockStatement(interpreter, i, { TokenType::Elseif, TokenType::Else, TokenType::End }));
            
            pairs.emplace_back(condition, then);
        }
        while (interpreter.GetToken(i).type == TokenType::Elseif);
        
        if (interpreter.GetToken(i).type == TokenType::Else)
        {
            ++i;
            elseStatements = std::unique_ptr<BlockStatement>(new BlockStatement(interpreter, i, { TokenType::End }));
        }
        
		++i;
	}

	void IfStatement::Evaluate(Interpreter& interpreter) const
	{
        for (auto& pair : pairs)
        {
            auto temp = pair.condition->Evaluate(interpreter);
            BoolValue* value = dynamic_cast<BoolValue*>(temp.get());

            if (value == nullptr)
            {
                throw std::runtime_error("");
            }
            
            if (value->value)
            {
                pair.then->Evaluate(interpreter);
                return;
            }
        }

        elseStatements->Evaluate(interpreter);
	}

	void IfStatement::ToString(std::ostream& out) const
	{
		out << "if " << *pairs[0].condition << std::endl << *pairs[0].then;
        
        for (int i = 1; i < pairs.size(); i++)
        {
            out << "elseif " << *pairs[i].condition << std::endl << *pairs[i].then;
        }
        
        if (elseStatements)
        {
            out << "else" << std::endl << *elseStatements;
        }
        
        out << "end";
	}

    WhileStatement::WhileStatement(const Interpreter& interpreter, int& i)
    {
        ++i;
        condition = Expression::Parse(Priority::Lowest, interpreter, i);
        ++i;
        // make_uniqueでinitializer_listが使えねえからこうなった(きったねえ)
        then = std::unique_ptr<BlockStatement>(new BlockStatement(interpreter, i, { TokenType::End }));
        ++i;
    }

    void WhileStatement::Evaluate(Interpreter& interpreter) const
    {
        while (true)
        {
            auto temp = condition->Evaluate(interpreter);
            BoolValue* value = dynamic_cast<BoolValue*>(temp.get());

            if (value == nullptr)
            {
                throw std::runtime_error("");
            }

            if (value->value)
            {
                then->Evaluate(interpreter);
            }
            else
            {
                break;
            }
        }
    }

    void WhileStatement::ToString(std::ostream& out) const
    {
        out << "while " << *condition << std::endl << *then << "end";
    }

	PrintStatement::PrintStatement(const Interpreter& interpreter, int& i)
	{
		++i;
		expression = Expression::Parse(Priority::Lowest, interpreter, i);
		++i;
	}

	void PrintStatement::Evaluate(Interpreter& interpreter) const
	{
		std::cout << *expression->Evaluate(interpreter) << std::endl;
	}

	void PrintStatement::ToString(std::ostream& out) const
	{
		out << "print " << *expression;
	}

	ExpressionStatement::ExpressionStatement(const Interpreter& interpreter, int& i)
	{
		expression = Expression::Parse(Priority::Lowest, interpreter, i);
		++i;
	}

	void ExpressionStatement::Evaluate(Interpreter& interpreter) const
	{
		expression->Evaluate(interpreter);
	}

	void ExpressionStatement::ToString(std::ostream& out) const
	{
		out << *expression;
	}

    IntStatement::IntStatement(const Interpreter& interpreter, int& i)
    {
        ++i;
        if (interpreter.GetToken(i).type == TokenType::Identifier)
        {
            identifier = interpreter.GetToken(i).literal;
        }
        ++i;
    }

    void IntStatement::Evaluate(Interpreter& interpreter) const
    {
        interpreter.variables.emplace(identifier, std::make_shared<IntValue>(0));
    }

    void IntStatement::ToString(std::ostream& out) const
    {
        out << "int " << identifier;
    }
}
