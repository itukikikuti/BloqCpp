#pragma once

namespace bloq
{
	enum class TokenType
	{
		Eof,
		NewLine,
		Assign,
		Percent,
		Equal,
		Less,
		Increment,
		If,
		Else,
		Elseif,
		While,
		End,
		Int,
		IntLiteral,
		StringLiteral,
		Print,
		Identifier,
	};

	struct Token
	{
		Token(TokenType type, std::string literal) :
            type(type),
            literal(literal)
        {
        }
        TokenType type;
        std::string literal;
	};

	inline std::ostream& operator<<(std::ostream& out, const Token& token)
	{
		out << token.literal;
		return out;
	}
}
