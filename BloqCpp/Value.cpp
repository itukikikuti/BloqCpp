#include "Header.hpp"

namespace bloq
{
	std::ostream& operator<<(std::ostream& out, const Value& value)
	{
		value.ToString(out);
		return out;
	}

	IntValue::IntValue(const int& value)
	{
		this->value = value;
	}

	void IntValue::ToString(std::ostream& out) const
	{
		out << value;
	}

	BoolValue::BoolValue(const bool& value)
	{
		this->value = value;
	}

	void BoolValue::ToString(std::ostream& out) const
	{
		out << std::boolalpha << value << std::noboolalpha;
	}

	StringValue::StringValue(const std::string& value)
	{
		this->value = value;
	}

	void StringValue::ToString(std::ostream& out) const
	{
		out << value.substr(1, value.length() - 2);
	}
}
