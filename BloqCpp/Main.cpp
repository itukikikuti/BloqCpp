#include "Header.hpp"

int main()
{
	std::ifstream file("Test01.bq");
	std::istreambuf_iterator<char> itr(file), end;
	std::string code(itr, end);
	bloq::Interpreter interpreter;
	interpreter.Tokenize(code);
	interpreter.Parse();
	interpreter.Evaluate();
}
