#include "Header.hpp"

namespace bloq
{
	std::unique_ptr<Expression> Expression::Parse(const Priority& priority, const Interpreter& interpreter, int& i)
	{
		std::unique_ptr<Expression> expression = nullptr;

		switch (interpreter.GetToken(i).type)
		{
		case TokenType::Increment:
			expression = std::make_unique<IncrementExpression>(interpreter, i);
			break;
		case TokenType::IntLiteral:
			expression = std::make_unique<IntLiteralExpression>(interpreter, i);
			break;
		case TokenType::StringLiteral:
			expression = std::make_unique<StringLiteralExpression>(interpreter, i);
			break;
		case TokenType::Identifier:
			expression = std::make_unique<IdentifierExpression>(interpreter, i);
			break;
		}

		while (interpreter.GetToken(i + 1).type != TokenType::NewLine and priority < GetInfixPriority(interpreter.GetToken(i + 1)))
		{
			++i;
			expression = InfixExpression::Parse(expression, interpreter, i);
		}

		return std::move(expression);
	}

    Priority Expression::GetInfixPriority(const Token& token)
    {
        switch (token.type)
        {
        case TokenType::Percent:
            return Priority::MulDivMod;
        case TokenType::Equal:
            return Priority::Equals;
        case TokenType::Less:
            return Priority::LessGreater;
        default:
            return Priority::Lowest;
        }
    }

	std::ostream& operator<<(std::ostream& out, const Expression& expression)
	{
		expression.ToString(out);
		return out;
	}

	std::unique_ptr<InfixExpression> InfixExpression::Parse(std::unique_ptr<Expression>& left, const Interpreter& interpreter, int& i)
	{
		std::unique_ptr<InfixExpression> expression = nullptr;

		switch (interpreter.GetToken(i).type)
		{
		case TokenType::Percent:
			expression = std::make_unique<ModExpression>(interpreter, i);
			break;
		case TokenType::Equal:
			expression = std::make_unique<EqualExpression>(interpreter, i);
			break;
		case TokenType::Less:
			expression = std::make_unique<LessExpression>(interpreter, i);
            break;
		}

		expression->left = std::move(left);

		Priority priority = GetInfixPriority(interpreter.GetToken(i));
		++i;
		expression->right = Expression::Parse(priority, interpreter, i);

		return std::move(expression);
	}

	IncrementExpression::IncrementExpression(const Interpreter& interpreter, int& i)
	{
		++i;
		right = Expression::Parse(Priority::Prefix, interpreter, i);
	}

	std::shared_ptr<Value> IncrementExpression::Evaluate(Interpreter& interpreter) const
	{
        auto temp = right->Evaluate(interpreter);
        IntValue* value = dynamic_cast<IntValue*>(temp.get());

        if (value == nullptr)
        {
            throw std::runtime_error("");
        }
        
        ++value->value;

        return temp;
	}

	void IncrementExpression::ToString(std::ostream& out) const
	{
		out << "++ " << *right;
	}

	IntLiteralExpression::IntLiteralExpression(const Interpreter& interpreter, int& i)
	{
		value = std::stoi(interpreter.GetToken(i).literal);
	}

	std::shared_ptr<Value> IntLiteralExpression::Evaluate(Interpreter& interpreter) const
	{
		return std::make_shared<IntValue>(value);
	}

	void IntLiteralExpression::ToString(std::ostream& out) const
	{
		out << value;
	}

	StringLiteralExpression::StringLiteralExpression(const Interpreter& interpreter, int& i)
	{
		value = interpreter.GetToken(i).literal;
	}

	std::shared_ptr<Value> StringLiteralExpression::Evaluate(Interpreter& interpreter) const
	{
		return std::make_shared<StringValue>(value);
	}

	void StringLiteralExpression::ToString(std::ostream& out) const
	{
		out << value;
	}

	IdentifierExpression::IdentifierExpression(const Interpreter& interpreter, int& i)
	{
		identifier = interpreter.GetToken(i).literal;
	}

	std::shared_ptr<Value> IdentifierExpression::Evaluate(Interpreter& interpreter) const
	{
        if (interpreter.variables.count(identifier) == 0)
            throw std::runtime_error("");
        
		return interpreter.variables[identifier];
	}

	void IdentifierExpression::ToString(std::ostream& out) const
	{
		out << identifier;
	}

	ModExpression::ModExpression(const Interpreter& interpreter, int& i)
	{
	}

	std::shared_ptr<Value> ModExpression::Evaluate(Interpreter& interpreter) const
	{
		auto ltemp = left->Evaluate(interpreter);
		auto rtemp = right->Evaluate(interpreter);
		IntValue* lvalue = dynamic_cast<IntValue*>(ltemp.get());
		IntValue* rvalue = dynamic_cast<IntValue*>(rtemp.get());

		if (lvalue == nullptr or rvalue == nullptr)
		{
            throw std::runtime_error("");
		}

		return std::make_shared<IntValue>(lvalue->value % rvalue->value);
	}

	void ModExpression::ToString(std::ostream& out) const
	{
		out << *left << " % " << *right;
	}

	EqualExpression::EqualExpression(const Interpreter& interpreter, int& i)
	{
	}

	std::shared_ptr<Value> EqualExpression::Evaluate(Interpreter& interpreter) const
	{
		auto ltemp = left->Evaluate(interpreter);
		auto rtemp = right->Evaluate(interpreter);
		IntValue* lvalue = dynamic_cast<IntValue*>(ltemp.get());
		IntValue* rvalue = dynamic_cast<IntValue*>(rtemp.get());

		if (lvalue == nullptr or rvalue == nullptr)
		{
            throw std::runtime_error("");
		}

		return std::make_shared<BoolValue>(lvalue->value == rvalue->value);
	}

	void EqualExpression::ToString(std::ostream& out) const
	{
		out << *left << " == " << *right;
	}

	LessExpression::LessExpression(const Interpreter& interpreter, int& i)
	{
	}

	std::shared_ptr<Value> LessExpression::Evaluate(Interpreter& interpreter) const
	{
		auto ltemp = left->Evaluate(interpreter);
		auto rtemp = right->Evaluate(interpreter);
		IntValue* lvalue = dynamic_cast<IntValue*>(ltemp.get());
		IntValue* rvalue = dynamic_cast<IntValue*>(rtemp.get());

		if (lvalue == nullptr or rvalue == nullptr)
		{
            throw std::runtime_error("");
		}

		return std::make_shared<BoolValue>(lvalue->value < rvalue->value);
	}

	void LessExpression::ToString(std::ostream& out) const
	{
		out << *left << " < " << *right;
	}
}
