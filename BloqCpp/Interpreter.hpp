#pragma once

namespace bloq
{
	class RootStatement;

	class Interpreter
	{
	public:
		Interpreter();
		void Tokenize(std::string code);
		void Parse();
		void Evaluate();
        Token GetToken(const int& i) const;
        std::map<std::string, std::shared_ptr<Value>> variables;

	private:
        struct TokenPattern
        {
            TokenPattern(const TokenType& tokenType, const std::string& pattern) :
                tokenType(tokenType),
                pattern(pattern)
            {
            }
            TokenType tokenType;
            std::string pattern;
        };
		Token ToToken(const std::string& literal);
        std::vector<TokenPattern> tokenPatterns;
        std::vector<Token> tokens;
        std::unique_ptr<RootStatement> root = nullptr;
	};
}
